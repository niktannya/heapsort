#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "list.h"
#include "student.h"
 
#define NMAX 20
 
int list::readl(int size, FILE *fp)
{
    list *p = this;
    p->read(fp);
    for (int i = 1; i < size; i++)
    {
        p->paste(fp);
        p = p->next;
    }
    return 0;
}
 
void list::print(int size)
{
    list *p = this;
    int max_size = (size < NMAX ? size : NMAX);
    for (int i = 0; p !=0 && i < max_size; i++)
    {
        printf("%s %d\n",p->get_name(),p->get_value());
        p = p->next;
    }
}
 
list& list::jump(int step)
{
    list *p = this;
    for ( ; step > 0 && p->next; step--)
        p = p->next;
    return *p;
}

